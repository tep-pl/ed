package pl.agh.ed

import java.io.FileInputStream
import java.util.Properties

import com.cybozu.labs.langdetect.DetectorFactory
import org.apache.spark.SparkConf
import org.apache.spark.streaming.twitter._
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.elasticsearch.spark._
import pl.agh.ed.util.LogUtils

import scala.io.Source
import scala.util.Try

object TwitterED {

   def main(args: Array[String]) {
     // set logger
     LogUtils.setStreamingLogLevels()

     // load credentials
     try {
       val prop = new Properties()
       prop.load(new FileInputStream("src/main/resources/credentials.properties"))

       System.setProperty("twitter4j.oauth.consumerKey", prop.getProperty("twitter.consumerKey"))
       System.setProperty("twitter4j.oauth.consumerSecret", prop.getProperty("twitter.consumerSecret"))
       System.setProperty("twitter4j.oauth.accessToken", prop.getProperty("twitter.accessToken"))
       System.setProperty("twitter4j.oauth.accessTokenSecret", prop.getProperty("twitter.accessTokenSecret"))
     } catch { case e: Exception =>
       e.printStackTrace()
       sys.exit(1)
     }

     val filtersSrc = Source.fromFile("src/main/resources/filters")
     val filters = filtersSrc.getLines().toArray

     // init spark streaming
     val sparkConf = new SparkConf().setAppName("TwitterED")
     val ssc = new StreamingContext(sparkConf, Seconds(1))
     val tweets = TwitterUtils.createStream(ssc, None, filters)

     // map tweets to DB
     tweets.print()
     tweets.foreachRDD{(rdd, time) =>
       rdd.map(t => {
         Map(
           "user"-> t.getUser.getScreenName,
           "created_at" -> t.getCreatedAt.toInstant.toString,
           "location" -> Option(t.getGeoLocation).map(geo => { s"${geo.getLatitude},${geo.getLongitude}" }),
           "text" -> t.getText,
           "hashtags" -> t.getHashtagEntities.map(_.getText),
           "retweet" -> t.getRetweetedStatus.getRetweetCount,
           "language" -> detectLanguage(t.getText)
         )
       }).saveToEs("twitter/tweet")
     }

     ssc.start()
     ssc.awaitTermination()
   }

   def detectLanguage(text: String) : String = {
      Try {
        val detector = DetectorFactory.create()
        detector.append(text)
        detector.detect()
      }.getOrElse("unknown")
   }
 }
